import fnmatch
import os
from ftplib import FTP
from json import dumps, load
from zipfile import ZipFile
# import load_static



def check_static(cfg, file_pattern):
  print('>>> GETTING STATIC FILES <<<')
  
  # set up connection string to the vec
  ftp = FTP(cfg['ftp']['host'])

  # login and get some files
  ftp.login()
  files = ftp.nlst()
  for file in files:
    if fnmatch.fnmatch(file, file_pattern):
      getfile = cfg['ftp']['local-dir']+'/'+file
      if not os.path.isfile(getfile):
        print('Retreiving '+file)
        ftp.retrbinary("RETR " + file, open(getfile, 'wb').write)
      else:
        print('File '+file+' already exists')

  ftp.quit()
  return

def get_dynamic(cfg, file_pattern):

  print('>>> GETTING LATEST DYNAMIC FILES <<<')

  # set up connection string to the vec
  host = cfg['ftp']['host']
  local_dir = cfg['ftp']['local-dir']
  ftp = FTP(host)

  # login and get some files
  ftp.login()
  files = ftp.nlst()

  # create a list comprehension to filter on matching files
  f_files = [file for file in files if fnmatch.fnmatch(file, file_pattern) == True ]
  print('>>> Last file is ' + f_files[-1])

  getfile = local_dir+'/'+f_files[-1]
  if not os.path.isfile(getfile):
    print('Retreiving '+f_files[-1])
    ftp.retrbinary("RETR " + f_files[-1], open(getfile, 'wb').write)
    zipfile = ZipFile(getfile)
    zipfile.extractall('./xml-data')
    
  else:
    print('File '+f_files[-1]+' already exists')

  ftp.quit()



  return

cfg_file = './main.json'

# if config file exists process ftp files
if os.path.isfile(cfg_file):
  with open(cfg_file,'r') as f:
    cfg = load(f)

  # check for chnages in static files (unlikely)
  file_pattern = cfg['static']['prefix']+'*'+cfg['static']['ext']
  check_static(cfg, file_pattern)

  # get latest lower house results
  file_pattern = cfg['dynamic']['prefix']+cfg['dynamic']['lowerH']+'*'+cfg['dynamic']['ext']
  get_dynamic(cfg, file_pattern)

  # get latest upper house results
  file_pattern = cfg['dynamic']['prefix']+cfg['dynamic']['upperH']+'*'+cfg['dynamic']['ext']
  get_dynamic(cfg, file_pattern)

else:
  print('<<< NO CONFIG FILE FOUND >>>')