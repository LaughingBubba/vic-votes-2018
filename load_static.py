import xml.etree.ElementTree as ET
import sqlite3

# initialise DB
def init_db(conn):

  c = conn.cursor()

  # Create districts table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS districts (
      id INTEGER PRIMARY KEY,
      name TEXT,
      evt_id INTEGER,
      evt_name TEXT);
    ''')

  # Create polling place table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS pollplace (
      id INTEGER PRIMARY KEY,
      name TEXT,
      access TEXT,
      updated TEXT DEFAULT "",
      vtf_tot INTEGER DEFAULT 0,
      vti_tot INTEGER DEFAULT 0,
      vtt_tot INTEGER DEFAULT 0,
      pd_id INTEGER,
      pd_name TEXT,
      evt_id INTEGER,
      evt_name TEXT);
    ''')

  # Create contests table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS contests (
      id INTEGER PRIMARY KEY,
      name TEXT,
      pd_id INTEGER,
      pd_name TEXT,
      declared INTEGER DEFAULT 0,
      npos INTEGER DEFAULT 0,
      enrolled INTEGER,
      ppe INTEGER DEFAULT 0,
      ppr INTEGER DEFAULT 0,
      updated TEXT DEFAULT "",
      prevupd TEXT DEFAULT "",
          
      vtf_tot INTEGER DEFAULT 0,
      vtf_ord INTEGER DEFAULT 0,
      vtf_post INTEGER DEFAULT 0,
      vtf_early INTEGER DEFAULT 0,
      vtf_mav INTEGER DEFAULT 0,
      vtf_prov INTEGER DEFAULT 0,
      vtf_abs INTEGER DEFAULT 0,

      vti_tot INTEGER DEFAULT 0,
      vti_ord INTEGER DEFAULT 0,
      vti_post INTEGER DEFAULT 0,
      vti_early INTEGER DEFAULT 0,
      vti_mav INTEGER DEFAULT 0,
      vti_prov INTEGER DEFAULT 0,
      vti_abs INTEGER DEFAULT 0,

      vtt_tot INTEGER DEFAULT 0,
      vtt_ord INTEGER DEFAULT 0,
      vtt_post INTEGER DEFAULT 0,
      vtt_early INTEGER DEFAULT 0,
      vtt_mav INTEGER DEFAULT 0,
      vtt_prov INTEGER DEFAULT 0,
      vtt_abs INTEGER DEFAULT 0,
      
      evt_id INTEGER,
      evt_name TEXT,
      house TEXT);
    ''')

  # Create candidates table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS candidates (
      id INTEGER PRIMARY KEY,
      name TEXT,
      ct_id INTEGER,
      ct_name TEXT,
      ballot_pos INTEGER,
      elected TEXT,
      updated TEXT DEFAULT "",
      vtf_tot INTEGER DEFAULT 0,
      vtf_ord INTEGER DEFAULT 0,
      vtf_post INTEGER DEFAULT 0,
      vtf_early INTEGER DEFAULT 0,
      vtf_mav INTEGER DEFAULT 0,
      vtf_prov INTEGER DEFAULT 0,
      vtf_abs INTEGER DEFAULT 0,
      independent TEXT,
      aff_id INTEGER,
      aff_name TEXT,
      pd_id INTEGER,
      evt_id INTEGER,
      evt_name TEXT,
      house TEXT);
    ''')
  # c.close()
  return

# get name space from tag name
def get_ns(name):
  i = name.find('}')+1
  if i > -1:
      return name[0:i]
  else:
      return name

# Parse and load district and polling locations
def locations(xml_file, conn):
  c = conn.cursor()
  tree = ET.parse(xml_file)
  root = tree.getroot()

  print('>> Printing location elements')
  for x0 in root:

    if (x0.tag).endswith('EventIdentifier'):
      evt_id = x0.attrib['Id']
      evt_name = x0[0].text
      # print('event {} {}'.format(evt_id,evt_name))
    
    if (x0.tag).endswith('PollingDistrict'):
      x0L = list(x0)

      for x1 in x0L:
        if (x1.tag).endswith('PollingDistrictIdentifier'):
          pd_id = x1.attrib['Id']
          pd_name = x1[0].text
          sql = '''
            INSERT OR REPLACE INTO 
              districts(id, name, evt_id, evt_name)
              VALUES({},"{}",{},"{}");
            '''.format(pd_id,pd_name,evt_id,evt_name)
          # print(sql)
          c.execute(sql)          
          # print('event {} {} district {} {}'.format(evt_id,evt_name,pd_id,pd_name ))
        
        if (x1.tag).endswith('PollingPlaces'):
          x1L = list(x1)

          for x2 in x1L:
            if (x2.tag).endswith('PollingPlace'):
              pl_id = x2[0].attrib['Id']
              pl_name = x2[0].attrib['Name']
              pl_access = x2[1].text
              sql = '''
                INSERT OR REPLACE INTO 
                  pollplace(id, name, access, pd_id, pd_name, evt_id, evt_name)
                  VALUES({},"{}","{}",{},"{}",{},"{}");
                '''.format(pl_id,pl_name,pl_access,pd_id,pd_name,evt_id,evt_name)
              # print(sql)
              c.execute(sql)                  
              # print('  locn {} {} access {}'.format(pl_id,pl_name,pl_access))

  conn.commit()
  # conn.close()
  return


# parse and load candidates and contests Tables from candidates XML
def candidates(xml_file, conn):

  c = conn.cursor()
  tree = ET.parse(xml_file)
  root = tree.getroot()
  ns = get_ns(root.tag)

  print('>> Printing root elements')
  for x0 in root:

    if x0.tag.lstrip(ns) == 'CandidateList':

      x0L = list(x0)
      for x1 in x0L:
        if x1.tag.lstrip(ns) == 'EventIdentifier':
          evt_id = x1.attrib['Id']
          evt_name = x1[0].text

        if x1.tag.lstrip(ns) == 'Election':
          x1L = list(x1)
          for x2 in x1L:
            if x2.tag.lstrip(ns) == 'ElectionIdentifier':
              if x2.attrib['Id'] == 'L':
                house = 'lower'
              else:
                house = 'upper'
              print('{e} {h} house'.format(h=house,e=evt_name))
            
            x2L = list(x2)
            for x3 in x2L:
              if x3.tag.lstrip(ns) == 'ContestIdentifier':
                ci=x3.attrib['Id']
                cn=x3[0].text
              
              if x3.tag.lstrip(ns) == 'PollingDistrictIdentifier':
                pd_id=x3.attrib['Id']
                pd_name = ''
                # pd_name=x3[0].text

              if x3.tag.lstrip(ns) == 'Enrolment':
                enrolled=x3.text
                # PUT contest record
                sql = '''
                  INSERT OR REPLACE INTO 
                    contests(id, name, pd_id, pd_name, enrolled, evt_id, evt_name, house)
                    VALUES({},"{}",{},"{}",{},{},"{}","{}");
                  '''.format(ci,cn,pd_id,pd_name,enrolled,evt_id,evt_name,house)
                # print(sql)
                c.execute(sql)
                # print('distID {pi} enrolled {en} contest {ci} {cn}'.format(h=house,e=evt_name,ci=ci,cn=cn,pi=pdi,en=enrolled))
              
              if x3.tag.lstrip(ns) == 'Candidate':
                indi=x3.attrib['Independent']
                can_id = x3[0].attrib['Id']
                can_name = x3[0][0].text
                elctd = x3[1].attrib['Historic']
                if indi == 'yes':
                  aff_id = 0
                  aff_name = 'INDEPENDENT'
                  bpos = x3[2].text
                else:
                  aff_id = x3[2][0].attrib['Id']
                  aff_name = x3[2][0][0].text
                  bpos = x3[3].text
                sql = '''
                  INSERT OR REPLACE INTO 
                    candidates(id, name, ct_id, ct_name, ballot_pos, elected,
                             independent, aff_id, aff_name, pd_id, 
                             evt_id, evt_name, house)
                    VALUES({},"{}",{},"{}",{},"{}","{}",{},"{}",{},{},"{}","{}");
                  '''.format(can_id,can_name,ci,cn,bpos,elctd,indi,aff_id,aff_name,pd_id,evt_id,evt_name,house)
                # print(sql)
                c.execute(sql)
                # print('   {} {} {} {} - {} {} {}'.format(elctd,bpos,can_id,can_name,indi,aff_id,aff_name))
  
  conn.commit()
  # conn.close()
  return

## MAINLINE

# Setup DB if necessary
conn = sqlite3.connect('./election.sqlite')
init_db(conn)

# Load data
locations('./xml-data/State2018MediaFilePollingLocations.xml',conn)
candidates('./xml-data/State2018MediaFileCandidates.xml',conn)

conn.close()