import xml.etree.ElementTree as ET
import sqlite3

# initialise DB
def init_db(conn):

  c = conn.cursor()

  # Create districts table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS districts (
      id INTEGER PRIMARY KEY,
      name TEXT,
      evt_id INTEGER,
      evt_name TEXT);
    ''')

  # Create candidate / polling place results table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS cpp_results (
      pp_id INTEGER,
      pp_name TEXT DEFAULT "",
      cn_id INTEGER,
      cn_name TEXT DEFAULT "",
      updated TEXT DEFAULT "",
      vtt_tot INTEGER DEFAULT 0,
      evt_id INTEGER,
      evt_name TEXT,
      PRIMARY KEY (pp_id, cn_id)
      );
    ''')

  # Create 2 candidate preferred / polling place results table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS tcp_pp_results (
      pp_id INTEGER,
      pp_name TEXT DEFAULT "",
      cn_id INTEGER,
      cn_name TEXT DEFAULT "",
      updated TEXT DEFAULT "",
      vtt_tot INTEGER DEFAULT 0,
      evt_id INTEGER,
      evt_name TEXT,
      PRIMARY KEY (pp_id, cn_id)
      );
    ''') 

  # Create 2 candidate preferred / contest results table
  c.execute(
    '''
    CREATE TABLE IF NOT EXISTS tcp_cn_results (
      ct_id INTEGER,
      ct_name TEXT DEFAULT "",
      cn_id INTEGER,
      cn_name TEXT DEFAULT "",
      updated TEXT DEFAULT "",

      vtt_tot INTEGER DEFAULT 0,
      vtt_ord INTEGER DEFAULT 0,
      vtt_post INTEGER DEFAULT 0,
      vtt_early INTEGER DEFAULT 0,
      vtt_mav INTEGER DEFAULT 0,
      vtt_prov INTEGER DEFAULT 0,
      vtt_abs INTEGER DEFAULT 0,

      evt_id INTEGER,
      evt_name TEXT,
      PRIMARY KEY (ct_id, cn_id)
      );
    ''')  
  return

def lh_results(xml_file, conn):
  c = conn.cursor()
  tree = ET.parse(xml_file)
  root = tree.getroot()

  print('>> LOWER house results')
  for x0 in root:

    if (x0.tag).endswith('EventIdentifier'):
      evt_id = x0.attrib['Id']
      evt_name = x0[0].text
      # print('event {} {}'.format(evt_id,evt_name))

    if (x0.tag).endswith('Election'):
      x0L = list(x0[1][0])

      for x1 in x0L:
        if (x1.tag).endswith('Contest'):

          x1L = list(x1)
          for x2 in x1L:
            if (x2.tag).endswith('ContestIdentifier'):
              ct_id=x2.attrib['Id']
              ct_name=x2[0].text

            if (x2.tag).endswith('PollingDistrictIdentifier'):
              pd_id=x2.attrib['Id']
              pd_name=x2[0].text
              sql = '''
                UPDATE contests 
                  set pd_name="{pn}"
                WHERE id={ci};
                '''.format(ci=ct_id,pn=pd_name)
              # print(sql)
              c.execute(sql)      
              conn.commit()   

            if (x2.tag).endswith('Declared'):
              if x2.text == 'true':
                dl = 1
              else:
                dl = 0
              sql = '''
                UPDATE contests 
                  set declared={dl}
                WHERE id={ci};
                '''.format(ci=ct_id,dl=dl)
              # print(sql)
              c.execute(sql)      
              conn.commit()  

            if (x2.tag).endswith('NumberOfPositions'):
              npos = x2.text
              sql = '''
                UPDATE contests 
                  set npos={npos}
                WHERE id={ci};
                '''.format(ci=ct_id,npos=npos)
              # print(sql)
              c.execute(sql)      
              conn.commit()  

            if (x2.tag).endswith('FirstPreferences'):
              ppe = x2.attrib['PollingPlacesExpected']
              ppr = x2.attrib['PollingPlacesReturned']
              upd = x2.attrib['Updated']
              sql = '''
                UPDATE contests 
                  set ppe={ppe}, ppr={ppr}, updated="{upd}"
                WHERE id={cti};
              '''.format(cti=ct_id,ppe=ppe,ppr=ppr,upd=upd)
              # print(sql)
              c.execute(sql)      
              conn.commit()

              fpList = list(x2)
              # print(' ppr: type:',type(ppr),' value:',ppr)
              for e in fpList:
                if (e.tag).endswith('Candidate'):
                  cList = list(e)
                  for e in cList:
                    
                    if (e.tag).endswith('CandidateIdentifier'):
                      cn_id = e.attrib['Id']

                    if (e.tag).endswith('Votes'):
                      vts = e.text
                      sql = '''
                      UPDATE candidates 
                        set vtf_tot={vts}, updated="{upd}"
                      WHERE id={cni};
                      '''.format(cni=cn_id,vts=vts,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit()

                    if (e.tag).endswith('VotesByType'):
                      ord = e[0].text
                      pst = e[1].text
                      vte = e[2].text
                      mav = e[3].text
                      prv = e[4].text
                      abs = e[5].text
                      sql = '''
                        UPDATE candidates 
                          set 
                            vtf_ord={ord}, vtf_post={pst}, vtf_early={vte},
                            vtf_mav={mav}, vtf_prov={prv}, vtf_abs={abs}, updated="{upd}"
                        WHERE id={cni};
                        '''.format(cni=cn_id,ord=ord,pst=pst,vte=vte,mav=mav,prv=prv,abs=abs,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit() 

                if (e.tag).endswith('Formal'):
                  fList = list(e)
                  for e in  fList:
                    if (e.tag).endswith('Votes'):
                      vts = e.text
                      sql = '''
                      UPDATE contests 
                        set vtf_tot={vts}, updated="{upd}"
                      WHERE id={cti};
                      '''.format(cti=ct_id,vts=vts,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit()

                    if (e.tag).endswith('VotesByType'):
                      ord = e[0].text
                      pst = e[1].text
                      vte = e[2].text
                      mav = e[3].text
                      prv = e[4].text
                      abs = e[5].text
                      sql = '''
                        UPDATE contests 
                          set 
                            vtf_ord={ord}, vtf_post={pst}, vtf_early={vte},
                            vtf_mav={mav}, vtf_prov={prv}, vtf_abs={abs}, updated="{upd}"
                        WHERE id={cti};
                        '''.format(cti=ct_id,ord=ord,pst=pst,vte=vte,mav=mav,prv=prv,abs=abs,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit() 

                if (e.tag).endswith('Informal'):
                  fList = list(e)
                  for e in  fList:
                    if (e.tag).endswith('Votes'):
                      vts = e.text
                      sql = '''
                        UPDATE contests 
                          set 
                            vti_tot={vts}, updated="{upd}"
                        WHERE id={ci};
                        '''.format(ci=ct_id,vts=vts,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit() 

                    if (e.tag).endswith('VotesByType'):
                      ord = e[0].text
                      pst = e[1].text
                      vte = e[2].text
                      mav = e[3].text
                      prv = e[4].text
                      abs = e[5].text
                      sql = '''
                        UPDATE contests 
                          set 
                            vti_ord={ord}, vti_post={pst}, vti_early={vte},
                            vti_mav={mav}, vti_prov={prv}, vti_abs={abs}, updated="{upd}"
                        WHERE id={ci};
                        '''.format(ci=ct_id,ord=ord,pst=pst,vte=vte,mav=mav,prv=prv,abs=abs,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit() 

                if (e.tag).endswith('Total'):
                    fList = list(e)
                    for e in  fList:
                      if (e.tag).endswith('Votes'):                    
                        vts = e.text
                        sql = '''
                          UPDATE contests 
                            set 
                              vtt_tot={vts}, updated="{upd}"
                          WHERE id={ci};
                          '''.format(ci=ct_id,vts=vts,upd=upd)
                        # print(sql)
                        c.execute(sql)      
                        conn.commit() 

                      if (e.tag).endswith('VotesByType'):
                        ord = e[0].text
                        pst = e[1].text
                        vte = e[2].text
                        mav = e[3].text
                        prv = e[4].text
                        abs = e[5].text
                        sql = '''
                          UPDATE contests 
                            set 
                              vtt_ord={ord}, vtt_post={pst}, vtt_early={vte},
                              vtt_mav={mav}, vtt_prov={prv}, vtt_abs={abs}, updated="{upd}"
                          WHERE id={ci};
                          '''.format(ci=ct_id,ord=ord,pst=pst,vte=vte,mav=mav,prv=prv,abs=abs,upd=upd)
                        # print(sql)
                        c.execute(sql)      
                        conn.commit()                         

            if (x2.tag).endswith('TwoCandidatePreferred'):
              tpList = list(x2)
              # print('> TCP')
              for e in tpList:
                if (e.tag).endswith('Candidate'):
                  cList = list(e)
                  for e in cList:
                    
                    if (e.tag).endswith('CandidateIdentifier'):
                      cn_id = e.attrib['Id']

                    if (e.tag).endswith('Votes'):
                      vts = e.text
                      sql = '''
                        INSERT OR REPLACE INTO tcp_cn_results
                          (vtt_tot, updated, cn_id, ct_id, evt_id, evt_name, cn_name, ct_name) 
                        VALUES({vts}, "{upd}", {cni}, {cti}, {ei}, "{en}",
                          (select name from candidates where id={cni}),
                          (select name from contests where id={cti})                        
                        );
                      '''.format(cti=ct_id,cni=cn_id,vts=vts,upd=upd,ei=evt_id,en=evt_name)   
                      c.execute(sql)      
                      conn.commit()

                    if (e.tag).endswith('VotesByType'):
                      ord = e[0].text
                      pst = e[1].text
                      vte = e[2].text
                      mav = e[3].text
                      prv = e[4].text
                      abs = e[5].text
                      sql = '''
                        UPDATE tcp_cn_results 
                          set 
                            vtt_ord={ord}, vtt_post={pst}, vtt_early={vte},
                            vtt_mav={mav}, vtt_prov={prv}, vtt_abs={abs}, updated="{upd}"
                        WHERE cn_id={cni} and ct_id={cti};
                        '''.format(cni=cn_id,cti=ct_id,ord=ord,pst=pst,vte=vte,mav=mav,prv=prv,abs=abs,upd=upd)
                      # print(sql)
                      c.execute(sql)      
                      conn.commit() 

                  # print('ct_id: ',ct_id)
                  if (e.tag).endswith('Votes'):
                    fList = list(e) 
                    vts = e.text
                    sql = '''
                      UPDATE tcp_cn_results
                        set 
                          vtt_tot={vts}, updated="{upd}"
                      WHERE cn_id={cni} and ct_id={cti};
                      '''.format(cni=cn_id,cti=ct_id,vts=vts,upd=upd)
                    # print(sql)
                    c.execute(sql)      
                    conn.commit()                   
    
            if (x2.tag).endswith('PollingPlaces'):
              pList = list(x2)
              for e in pList:
                
                if (e.tag).endswith('PollingPlace'):
                  upd = e.attrib['Updated']
                  pp_id = e[0].attrib['Id']
                  pList = list(e)
                  for e in pList:
                    if (e.tag).endswith('FirstPreferences'):
                      fpList = list(e)
                      # print(' ppr: type:',type(ppr),' value:',ppr)
                      for e in fpList:
                        if (e.tag).endswith('Candidate'):
                          cn_id = e[0].attrib['Id']
                          vts = e[1].text
                          sql = '''
                            INSERT OR REPLACE INTO cpp_results
                              (vtt_tot, updated, cn_id, pp_id, evt_id, evt_name, cn_name, pp_name) 
                            VALUES({vts}, "{upd}", {cni}, {ppi}, {ei}, "{en}", 
                              (select name from candidates where id={cni}),
                              (select name from pollplace where id={ppi})
                              );
                            '''.format(ppi=pp_id,cni=cn_id,vts=vts,upd=upd,ei=evt_id,en=evt_name)
                          # print(sql)
                          c.execute(sql)      
                          conn.commit()

                        if (e.tag).endswith('Formal'):
                          vts = e[0].text
                          sql = '''
                            UPDATE pollplace
                              set vtf_tot={vts}, updated="{upd}"
                            WHERE id={ppi};
                            '''.format(ppi=pp_id,vts=vts,upd=upd)
                          # print(sql)
                          c.execute(sql)      
                          conn.commit()

                        if (e.tag).endswith('Informal'):
                          vts = e[0].text
                          sql = '''
                            UPDATE pollplace
                              set vti_tot={vts}, updated="{upd}"
                            WHERE id={ppi};
                            '''.format(ppi=pp_id,vts=vts,upd=upd)
                          # print(sql)
                          c.execute(sql)      
                          conn.commit()

                        if (e.tag).endswith('Total'):
                          vts = e[0].text
                          sql = '''
                            UPDATE pollplace
                              set vtt_tot={vts}, updated="{upd}"
                            WHERE id={ppi};
                            '''.format(ppi=pp_id,vts=vts,upd=upd)
                          # print(sql)
                          c.execute(sql)      
                          conn.commit()

                    if (e.tag).endswith('TwoCandidatePreferred'):
                      upd = e.attrib['Updated']
                      fpList = list(e)
                      # print(' ppr: type:',type(ppr),' value:',ppr)
                      for e in fpList:
                        if (e.tag).endswith('Candidate'):
                          cn_id = e[0].attrib['Id']
                          vts = e[1].text
                          sql = '''
                            INSERT OR REPLACE INTO tcp_pp_results
                              (vtt_tot, updated, cn_id, pp_id, evt_id, evt_name, cn_name, pp_name) 
                            VALUES({vts}, "{upd}", {cni}, {ppi}, {ei}, "{en}",
                              (select name from candidates where id={cni}),
                              (select name from pollplace where id={ppi})                            
                              );
                            '''.format(ppi=pp_id,cni=cn_id,vts=vts,upd=upd,ei=evt_id,en=evt_name)
                          # print(sql)
                          c.execute(sql)      
                          conn.commit()                      
  return

## MAINLINE

# Setup DB if necessary
conn = sqlite3.connect('./election.sqlite')
init_db(conn)

# Load data
lh_results('./xml-data/State2018mediafilelitepplh_20181208_120022.xml',conn)

conn.close()